#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#include <iostream>
#include <string>
#include "Helper.h"
#pragma comment(lib, "User32.lib")

using namespace std;

typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
void DisplayErrorBox(LPTSTR lpszFunction);

int main()
{
	Helper *t = new Helper();
	DWORD n;
	TCHAR v[2048];
	string command = " ";
	string temp = "";	
	vector<string> te;
	LARGE_INTEGER li;
	HANDLE src;
	WIN32_FIND_DATA fd;

	while (command != "exit")
	{
		cout << "-- ";
		getline(cin,command);
		t->trim(command);
		te = t->get_words(command);
		if (te[0] == "pwd")
		{
			n = GetCurrentDirectoryA(2048,v);
			if (n == 0)
			{
				cout << "operation failed" << endl;
			}
			else if (n > 2048)
			{
				cout << "buffer too small" << endl;
			}
			else
			{
				cout << v << endl;
			}
		}
		else if (te[0] == "cd")
		{
			if (te[1] == "..")
			{
				n = GetCurrentDirectoryA(2048, v);
				temp = string(v);
				te = t->get_words(temp);
				temp = temp.substr(0, temp.find_last_of("\\"));
				
				if (!SetCurrentDirectory(temp.c_str()))
				{
					cout << "Path didn't change" << endl;
				}
				else
				{
					cout << "Path changed" << endl;
				}
			}
			else
			{
				if (!SetCurrentDirectory(te[1].c_str()))
					cout << "Path didn't change" << endl;
				else
				{
					cout << "Path changed" << endl;
				}
			}
		}
		else if (te[0] == "create")
		{
			src = CreateFile(te[1].c_str(), GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
			if (src == INVALID_HANDLE_VALUE)
			{
				cout << "unable to create file";
			}
		}
		else if (te[0] == "secret")
		{
			HINSTANCE dllFile = LoadLibrary(TEXT("Secret.dll"));
			typedef int(*TheAnswerToLifeTheUniverseAndEverything)(void);
			FARPROC ptest = GetProcAddress(HMODULE(dllFile), "TheAnswerToLifeTheUniverseAndEverything");

			TheAnswerToLifeTheUniverseAndEverything ptr;
			ptr = TheAnswerToLifeTheUniverseAndEverything(ptest);
			cout << ptr() << endl;
			FreeLibrary(dllFile);

		}
		else if (te[0] == "ls")
		{
			n = GetCurrentDirectoryA(2048, v);
			StringCchCat(v,2048,TEXT("\\*"));
			src = FindFirstFile(v, &fd);

			if (src == INVALID_HANDLE_VALUE)
			{
				DisplayErrorBox(TEXT("FindFirstFile"));
			}

			cout << fd.cFileName << endl;

			do
			{
				if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					cout << (TEXT("  %s   <DIR>\n"), fd.cFileName) << endl;
				}
				else
				{
					li.LowPart = fd.nFileSizeLow;
					li.HighPart = fd.nFileSizeHigh;
					cout << (TEXT("  %s   %ld bytes\n"), fd.cFileName, li.QuadPart) << endl;
				}
			} while (FindNextFile(src, &fd) != 0);

		}
		else if (te[0].find(".exe"))
		{
			n = GetCurrentDirectoryA(2048, v);
			StringCchCat(v, 2048, TEXT("\\"));
			StringCchCat(v, 2048, TEXT(te[0].c_str()));
			src = ShellExecute(NULL, "open", v, NULL, NULL, SW_SHOWDEFAULT);
		}
		else
		{
			cout << "command not found" << endl;
		}
	}

	return 0;
}


void DisplayErrorBox(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message and clean up

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}
